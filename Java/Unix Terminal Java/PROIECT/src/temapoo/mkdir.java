/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author HoriaAlexandru
 */
class mkdir extends WriteCommand
{
    public mkdir(String params)
    {
        super.checkPermission(MySingleton.currentFolder());
        if (super.hasWritePermission)
        {
            Folder fol;
            Vector<String> args = new Vector<String>();
            StringTokenizer st = new StringTokenizer(params);
            while (st.hasMoreTokens())
                args.add(st.nextToken());
            
            for (int i = 0; i < args.size(); i++)
            {
                fol = new Folder(args.elementAt(i));
                fol.setOwner(MySingleton.currentUser());
                fol.permission().setRead(true);
                fol.permission().setWrite(true);
                execute(fol);

            }
        }
    }
    
    public static void createFileSystem(String name)
    {
        Folder fol = new Folder(name);
        fol.setOwner(MySingleton.currentUser());
        fol.permission().setRead(true);
        fol.permission().setWrite(true);
        MySingleton.setCurrentFolder(fol);
        fol.parent = null;
    }
    
    public void execute(Repository rep)
    {
        rep.accept(this);
    }
    
    public void execute(Folder f) 
    {
        f.setParent(MySingleton.currentFolder());
        MySingleton.currentFolder().add(f);
        toAdd += "\n< Success";
    }

    public void execute(File f) 
    {
    }
}
