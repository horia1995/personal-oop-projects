/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author HoriaAlexandru
 */
class ls extends ReadCommand 
{
    boolean isRecursive = false;
    boolean isListAll = false;
    boolean isRegex = false;
 
    
    public ls(String params)
    {
        Vector<String> args = new Vector<String>();
        StringTokenizer st = new StringTokenizer(params);
        while (st.hasMoreTokens())
            args.add(st.nextToken());
        
        String name;
        String arg;

        if (args.size() == 0)
            this.execute(MySingleton.currentFolder());
        
        boolean found = false;
        
        if (args.size() == 1)
            if (args.elementAt(0).startsWith("-"))
            {
                parse(args.elementAt(0));
                execute(MySingleton.currentFolder());
            }
            else
                for (Data repo : MySingleton.currentFolder().Children())
                    if (repo.name().equals(args.elementAt(0)))
                    {
                        found = true;
                        execute(repo);
                    }
                        
        if (args.size() == 2)
        {
            name = args.elementAt(0);
            arg = args.elementAt(1);

            for (Data repo : MySingleton.currentFolder().Children())
                if (repo.name().equals(name))
                {
                    found = true;
                    arg.replaceFirst("^ *", "");
                    parse(arg);
                    this.execute(repo);
                }
        }
    }
    
    public void parse(String arg)
    {
        if (arg.equals("-ar") || arg.equals("-ra"))
        {
            isRecursive = true;
            isListAll = true;
        }
        
        if (arg.equals("-a"))
        {
            isListAll = true;
        }
        
        if (arg.equals("-r"))
        {
            isRecursive = true;
        }
    }

    public void execute(Repository rep)
    {
        rep.accept(this);
    }

    
    public void execute(Folder f) {
        super.checkPermission(f);
        if (super.hasReadPermission)
        {
            for (Data repo : f.Children()) 
            {
                toAdd += "\n< " + repo.name() + " ";
                if (isListAll)
                {
                    toAdd += repo.allProperties();
                    if (MySingleton.currentUser().type() == 0)
                        toAdd += " -rw-";
                    else
                        if (repo.permission().owner().equals
                                (MySingleton.currentUser()))
                        {
                        toAdd += " -";
                            if (repo.permission().readPermission())
                                toAdd += "r";
                            if (repo.permission().writePermission())
                                toAdd += "w";
                            toAdd +="-";
                        }
                }
            }
            
            if (this.isRecursive)
                for (Data repo : f.Children())
                {
                    if (repo instanceof Folder && !((Folder)repo).Children().isEmpty()) 
                    {
                        toAdd += "\n ./" + repo.name();
                        execute((Folder) repo);
                    }
                }
        }
    }

    public void execute(File f) 
    {
        toAdd += "\n< " + f.name() + " is not a folder";
    }

}
