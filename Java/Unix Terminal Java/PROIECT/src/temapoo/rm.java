    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

/**
 *
 * @author HoriaAlexandru
 */

class rm extends WriteCommand
{
    public rm(String path) throws MyInvalidPathException
    {
        boolean found = false;
        Folder toReturnTo = MySingleton.currentFolder();
        String[] args = path.split("/");
        String toRemove = args[args.length - 1];
        String realPath = "";
        for (int i = 0; i < args.length - 1; i++)
            realPath += args[i];

        new cd(realPath);
        Folder currentFolder = MySingleton.currentFolder();
        for (int i = 0; i < currentFolder.Children().size(); i++)
        {
            if (currentFolder.Children().get(i).name().equals(toRemove))
            {
                found = true;
                execute(MySingleton.currentFolder().Children().get(i));
            }
            
        }
        if (!found)
            toAdd += "\n< File/Folder does not exist";
        
        MySingleton.setCurrentFolder(toReturnTo);
    }
    
    public void execute(Repository rep)
    {
        rep.accept(this);
    }
    

    
    public void execute(File f) 
    {
        super.checkPermission(f);
        if (super.hasWritePermission)
        {
            toAdd += "\n< Success";
            MySingleton.currentFolder().remove(f);
        }
    }
    
    public void execute(Folder f) 
    {
        super.checkPermission(f);
        if (super.hasWritePermission)
        {
            if (f.Children().isEmpty())
            {
                MySingleton.currentFolder().remove(f);
                toAdd += "\n< Success";
            }
            else
                toAdd += "\n< Folder is not empty";
        }
    }
}
