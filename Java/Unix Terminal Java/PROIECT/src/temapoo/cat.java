/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

/**
 *
 * @author HoriaAlexandru
 */
public class cat extends ReadCommand 
{
    public cat(String params)
    {
        for (Data repo : MySingleton.currentFolder().Children())
            if (repo.name().equals(params))
                execute(repo);
    }
    
    public void execute(Repository rep)
    {
        rep.accept(this);
    }
    
    public void execute(Folder f) 
    {
        toAdd += "\n< This is not a file";
    }

    public void execute(File f) 
    {
        super.checkPermission(f);
        if (super.hasReadPermission)
        {
            toAdd += "\n< " + f.getContent();
        }
    }
}
