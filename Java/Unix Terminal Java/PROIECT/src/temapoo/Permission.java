/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

/**
 *
 * @author HoriaAlexandru
 */
class Permission
{
    private boolean read = false;
    private boolean write = false;
    private User owner;
    
    public Permission(User owner)
    {
        this.owner = owner;
    }
    
    public boolean readPermission()
    {
        return this.read;
    }
    
    public void setRead(boolean read)
    {
        this.read = read;
    }
    
    public void setWrite(boolean write)
    {
        this.write = write;
    }
    
    public boolean writePermission()
    {
        return this.write;
    }
    
    public User owner()
    {
        return this.owner;
    }
}
