package temapoo;

import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.Vector;

class File extends Data 
{
    private int size;
    private String content;
    private String type;
    
    public File(String name)
    {
        super(name);
        this.content = UUID.randomUUID().toString();
        
        String[] types = {"text", "binary"};
        Random random = new Random();
        int select = random.nextInt(types.length);
        this.type = types[select]; 
    }
    
    public String getContent()
    {
        return this.content;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public void setSize(int size)
    {
        this.size = size;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
    public String type()
    {
        return this.type;
    }

    @Override
    public float size() 
    {
        return this.size;
    }

    @Override
    public void setParent(Data repo) 
    {
        this.parent = repo;
    }
    
    @Override
    public void accept(Command c) 
    {
        c.execute(this);
    }

    @Override
    public String allProperties() 
    {
        return this.WhenCreated() + " " + this.size() + " " + this.type();
    }

}