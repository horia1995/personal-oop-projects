/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author HoriaAlexandru
 */
public class Manager
{
    static Vector<User> users = new Vector<User>();
    static String toAdd = "";
    
    public Manager()
    {
        users.add(new User("root", "rootpass", "root", "root", 0));
        users.add(new User("guest", "guest", "", "", 2));
        MySingleton.setCurrentUser(users.elementAt(1));
    }
    
    public static void newuser(String params)
    {
        
        Vector<String> args = new Vector<String>();
        StringTokenizer st = new StringTokenizer(params);
        while (st.hasMoreTokens())
            args.add(st.nextToken());
        
        if (args.size() == 4)
        {
            String username = args.elementAt(0);
            String password = args.elementAt(1);
            String firstName = args.elementAt(2);
            String lastName = args.elementAt(3);

            users.add(new User(username, password, firstName, lastName, 1));
            toAdd = "\n< Success";
        }
        else
            toAdd = "\n< Not enough parameters";
    }
    
    public static void echo(String params)
    {
        if (!params.contains(">"))
        {
            String[] whatToEcho = params.split("\"");
            toAdd = "\n< ";
            if (whatToEcho.length == 1 || whatToEcho.length == 2)
            for (String param : whatToEcho)
                toAdd += param;
        }
    }
    
    public static void login(String params)
    {
        Vector<String> args = new Vector<String>();
        StringTokenizer st = new StringTokenizer(params);
        while (st.hasMoreTokens())
            args.add(st.nextToken());

        if (args.size() == 2)
        {
            String username = args.elementAt(0);
            String password = args.elementAt(1);

            boolean found = false;
            for (User user : users)
            {
                if (user.username().equals(username) && user.password().equals(password))
                {
                    logout();
                    found = true;
                    toAdd = "\n< Success";
                    MySingleton.setCurrentUser(user);
                    user.setLastLogged();
                }
            }
            if (!found)
                toAdd = "\n< Failed";
        }
        else
            toAdd ="\n< Not enough parameters. Username and password required";
    }
    
    public static void logout()
    {
        if (MySingleton.currentUser().type() != 2)
        {
            MySingleton.setCurrentUser(users.elementAt(1));
            MySingleton.currentUser().setLastLogged();
            toAdd = "\n< You are now guest";
        }
        else
            toAdd = "\n< You are already guest";
    }
    
    public static String userinfo()
    {
        User user = MySingleton.currentUser();
        toAdd = "\n< UserName : " + user.username() + ", FirstName : " +
                user.surname() + ", LastName : " + user.lastName() + 
                ", Created : " + user.timeCreated()
                + ", LastLogin : " + user.whenLastLogged();
        return toAdd;
    }
}
    
