/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

/**
 *
 * @author HoriaAlexandru
 */
interface Command 
{
    void execute(Repository repo);
    void execute(File f);
    void execute(Folder f);
}

abstract class ReadCommand implements Command
{
    boolean hasReadPermission = false;
    public String toAdd = "";

    
    public void checkPermission(Data rep)
    {
        if (MySingleton.currentUser().type() != 2 &&
                ((rep.permission().readPermission()
                && rep.permission().owner().equals(MySingleton.currentUser()))
                || MySingleton.currentUser().type() == 0
                || MySingleton.currentFolder().name().equals("forall")))
            this.hasReadPermission = true;
        else
            toAdd += "\n< Permission denied";
    }
}

abstract class WriteCommand implements Command
{
    boolean hasWritePermission = false;
    public String toAdd = "";
    
    public void checkPermission(Data rep)
    {
        if (MySingleton.currentUser().type() != 2 && 
                ((rep.permission().writePermission()
                && rep.permission().owner().equals(MySingleton.currentUser()))
                || MySingleton.currentUser().type() == 0
                || MySingleton.currentFolder().name().equals("forall")))
            this.hasWritePermission = true;
        else
            toAdd += "\n< Permission denied";
    }
    
}
