My Java implementation of a "Unix Terminal Simulator" using Design Patterns

I offered a test model file with commands that are supposed to show the functionalities.

The implemented commands are : cat, cd, ls, mkdir, pwd, rm, touch, userinfo, newuser, login, logout.

I also implemented the three user states : guest, logged user, root, each with different permissions.
The file/folder hierarchy is also simulated