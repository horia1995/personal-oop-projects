package temapoo;

import java.util.LinkedList;

class Folder extends Data {

    LinkedList<Data> children = new LinkedList<Data>();

    public Folder(String name) 
    {
        super(name);
    }

    @Override
    public float size() 
    {
        int count = 0;
        for (Data repo : this.Children())
            count += repo.size();
        return count;
    }

    public void add(Data repo) {
        children.add(repo);
        repo.setParent(this);
    }

    public void remove(Data repo) {
        children.remove(repo);
    }

    public LinkedList<Data> Children() {
        return this.children;
    }

    @Override
    public void accept(Command c) 
    {
        c.execute(this);
    }

    @Override
    public void setParent(Data repo) 
    {
        this.parent = repo;
    }

    @Override
    public String allProperties() 
    {
        return this.WhenCreated() + " " + this.size();
    }
}
