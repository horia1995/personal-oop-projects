/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;

/**
 *
 * @author HoriaAlexandru
 */
class MyLayout implements LayoutManager {

    public MyLayout() 
    {}

    public void addLayoutComponent(String name, Component comp)
    {}

    public void removeLayoutComponent(Component comp) 
    {}

    public Dimension preferredLayoutSize(Container parent) 
    {
        Dimension dim = new Dimension(0, 0);

        Insets insets = parent.getInsets();
        dim.width = 500 + insets.left + insets.right;
        dim.height = 400 + insets.top + insets.bottom;

        return dim;
    }

    public Dimension minimumLayoutSize(Container parent) 
    {
        Dimension dim = new Dimension(0, 0);
        return dim;
    }

    @Override
    public void layoutContainer(Container parent) 
    {
        Insets insets = parent.getInsets();
        Component c;

        c = parent.getComponent(0);
        if (c.isVisible()) 
            c.setBounds(insets.left + 0, insets.top + 0, 1100, 600);

        c = parent.getComponent(1);
        if (c.isVisible()) 
            c.setBounds(insets.left + 0, insets.top + 600, 550, 125);

        c = parent.getComponent(2);
        if (c.isVisible()) 
            c.setBounds(insets.left + 0, insets.top + 725, 550, 125);
 
        c = parent.getComponent(3);
        if (c.isVisible()) 
            c.setBounds(insets.left + 550, insets.top + 600, 550, 250);
    }
}

