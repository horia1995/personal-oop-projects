/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

/**
 *
 * @author HoriaAlexandru
 */
class MyInvalidPathException extends Exception
{
    public MyInvalidPathException()
    {
        super();
    }
    
    public MyInvalidPathException(String message)
    {
        super(message);
    }
}

class MyPathTooLongException extends MyInvalidPathException
{
    public MyPathTooLongException()
    {
        super();
    }
    
    public MyPathTooLongException(String message)
    {
        super(message);
    }
}
