/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import java.util.Vector;

/**
 *
 * @author HoriaAlexandru
 */
class cd extends ReadCommand
{
    
    public cd(String path) throws MyInvalidPathException
    {
        boolean canGoBack = true;
        boolean validPath = false;
        if (!path.equals(""))
        {
            String[] args = path.split("/");
            for (String arg : args)
            {
                if (arg.equals(".."))
                {
                    if (MySingleton.currentFolder().parent() == null)
                        canGoBack = false;
        
                    if (canGoBack)
                    {
                        validPath = true;
                        MySingleton.setCurrentFolder((Folder)MySingleton.currentFolder().parent);
                        toAdd += "\n< Success";
                    }
                    
                }
                else
                    for (Data repo : MySingleton.currentFolder().Children())
                        if (repo.name().equals(arg))
                        {
                            validPath = true;
                            execute(repo);
                        }
            }
            if (!validPath)
                throw new MyInvalidPathException("The path does not exist");
        }
    }
    
    public void execute(Repository rep)
    {
        rep.accept(this);
    }
    
    public void execute(Folder f) 
    {
        super.checkPermission(MySingleton.currentFolder());
        if (hasReadPermission)
        {
            MySingleton.setCurrentFolder(f);
            toAdd += "\n< Success";
        }
    }

    public void execute(File f) 
    {
        toAdd += "\n< " + f.name() + " is not a folder";
    }
}
