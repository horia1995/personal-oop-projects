/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import java.util.Collections;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HoriaAlexandru
 */
class pwd extends ReadCommand
{

    public pwd() 
    {
        execute(MySingleton.currentFolder());
    }
    
    public void execute(Repository rep)
    {
        rep.accept(this);
    }
    
    public void execute(Folder f) 
    {
        Vector<String> path = new Vector<String>();
        path.add(f.name());
        int pathLength = 0;
        
        Folder fol = f;
        while (fol.parent() != null)
        {
            fol = (Folder) fol.parent();
            path.add(fol.name());
            pathLength += fol.name().length();
        }

        System.out.println(pathLength);
        if (pathLength > 255)
            try {
                throw new MyPathTooLongException("Path is too long");
        } catch (MyPathTooLongException ex) {
            Logger.getLogger(pwd.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Collections.reverse(path);
        MySingleton.setCurrentFolder(f);
        
        toAdd = "\n< ";
        for (String elem : path)
            toAdd += elem + "/";
        
    }

    public void execute(File f) 
    {
        toAdd += "\n< " + f.name() + " is not a folder\n";
    }
   
}
