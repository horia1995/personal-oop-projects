/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;
import java.util.Date;
/**
 *
 * @author HoriaAlexandru
 */
class User {
    private String username;
    private String password;
    private String surname;
    private String lastName;
    private Date timeCreated = new Date();
    private Date whenLastLogged;
    private int type;
    
    public User(String username, String password, String surname,
            String lastName, int type)
    {
        this.username = username;
        this.password = password;
        this.surname = surname;
        this.lastName = lastName;
        this.type = type;
    }
    
    public void setUsername(String username)
    {
        this.username = username;
    }
    
    public String username()
    {
        return this.username;
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String password()
    {
        return this.password;
    }
    
    public void setSurname(String surname)
    {
        this.surname = surname;
    }
    
    public String surname()
    {
        return this.surname;
    }
    
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    
    public String lastName()
    {
        return this.lastName;
    }
    
    public void setLastLogged()
    {
        this.whenLastLogged = new Date();
    }
    
    public Date whenLastLogged()
    {
        return this.whenLastLogged;
    }
    
    public Date timeCreated()
    {
        return this.timeCreated;
    }
    
    public int type()
    {
        return this.type;
    }
}

