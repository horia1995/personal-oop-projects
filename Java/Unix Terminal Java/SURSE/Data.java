/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import java.util.Date;

/**
 *
 * @author HoriaAlexandru
 */

abstract class Data implements Repository 
{
    private final String name;
    protected Data parent = null;
    protected Permission permission;
    private final Date timeCreated;


    public Data(String name)
    {
        this.name = name;
        this.timeCreated = new Date();
    }
    
    public String name()
    {
        return this.name;
    }
    
    public Data parent()
    {
        return this.parent;
    }
    
    public Permission permission()
    {
        return this.permission;
    }
    
    public Date WhenCreated()
    {
        return this.timeCreated;
    }
    
    public void setOwner(User user)
    {
        this.permission = new Permission(user);
    }
    
    public abstract float size();
    public abstract void setParent(Data repo);
    public abstract String allProperties();
}



