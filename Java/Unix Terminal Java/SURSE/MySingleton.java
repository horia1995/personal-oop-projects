/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

/**
 *
 * @author HoriaAlexandru
 */
class MySingleton
{
    private static final MySingleton INSTANCE = new MySingleton();
    
    private MySingleton()
    {
        
    }
    
    private static User currentUser;
    private static Folder currentFolder;
    
    public static Folder currentFolder()
    {
        return MySingleton.currentFolder;
    }
    
    public static void setCurrentFolder(Folder currentFolder)
    {
        MySingleton.currentFolder = currentFolder;
    }
    
    public static User currentUser()
    {
        return MySingleton.currentUser;
    }
    
    public static void setCurrentUser(User curr)
    {
        MySingleton.currentUser = curr;
    }
    
    public static final MySingleton getInstance()
    {
        return INSTANCE;
    }
}
