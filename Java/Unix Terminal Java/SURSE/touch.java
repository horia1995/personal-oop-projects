/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import static java.nio.file.Files.size;
import java.util.StringTokenizer;
import java.util.Vector;
import static jdk.nashorn.internal.objects.NativeJava.type;
import static jdk.nashorn.internal.objects.NativeJava.type;

/**
 *
 * @author HoriaAlexandru
 */
class touch extends WriteCommand
{
    public touch(String params)
    {
        if (MySingleton.currentFolder() != null)
        {
            super.checkPermission(MySingleton.currentFolder());
            if (super.hasWritePermission)
            {
                Vector<String> args = new Vector<String>();
                StringTokenizer st = new StringTokenizer(params);
                while (st.hasMoreTokens())
                    args.add(st.nextToken());
               
                File fil = new File(args.elementAt(0));
                fil.setOwner(MySingleton.currentUser());
                
                String perm;
                if (args.size() > 1)
                    for (int i = 1; i < args.size(); i++)
                    {
                        if (args.elementAt(i).startsWith("-"))
                        {
                            perm = args.elementAt(i);
                            if (perm.matches("-rw-"))
                            {
                                fil.permission().setRead(true);
                                fil.permission().setWrite(true);
                            }
                            else if (perm.matches("-r-"))
                                fil.permission().setRead(true);
                            else if (perm.matches("-w-"))
                                fil.permission().setWrite(true);
                        }
                        else 
                        {
                            int size = Integer.parseInt(args.elementAt(i));
                            fil.setSize(size);

                        }
                    }
                execute(fil);
            }
        }
    }
    
    public void execute(Repository rep)
    {
        rep.accept(this);
    }
    
    public void execute(Folder f) 
    {
    }

    public void execute(File f) 
    {
        MySingleton.currentFolder().add(f);
        toAdd += "\n< Success";
    }
}
