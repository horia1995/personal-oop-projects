package temapoo;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.InputMap;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

class Terminal extends JFrame implements ListSelectionListener
{
    private JFrame frame = new JFrame("Terminal");
    private final JTextArea textArea;
    private final JScrollPane scrollPane;
    private final CommandFactory processor;
    private final String lineSeparator;
    private final Font font;
    private boolean extractFail = false;
    private boolean firstRow = true;
    private boolean formatted = false;
    
    DefaultListModel<String> listModel = new DefaultListModel<String>();
    JList list = new JList(listModel);
    JScrollPane scrollUI = new JScrollPane(list);   
    
    JDialog dialog = new JDialog(this);
    JScrollPane scrollDialog = new JScrollPane();
    private final JTextArea textAreaDialog;

    
    JTable table = new JTable(1, 1);
    JScrollPane scrollTable = new JScrollPane(table);
    DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
    String toEnter = "";
    
    public Terminal()
    {
        this.setLayout(new MyLayout());
        this.font = new Font("SansSerif", Font.BOLD, 15);
        this.lineSeparator = System.lineSeparator();
        this.processor = CommandFactory.getInstance();
        this.scrollPane = new JScrollPane();
        this.textArea = new JTextArea();
        this.add(scrollPane);
        this.setName("Terminal");
        
        scrollPane.setViewportView(textArea);

        textArea.setFont(font);
        textArea.setForeground(Color.WHITE);
        textArea.setBackground(Color.BLACK);
        textArea.addKeyListener(new KeyListener());
        ignoreArrows(textArea.getInputMap());

        
        this.textAreaDialog = new JTextArea(){
        @Override
        public Dimension getPreferredScrollableViewportSize() {
             return new Dimension(900, 500);
        }
        };
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(scrollDialog);
        scrollDialog.setViewportView(textAreaDialog);
        textAreaDialog.append("ECHO'S JDIALOG");
        textAreaDialog.setEditable(false);

        this.add(scrollUI);
        this.add(scrollTable);
        
        listModel.addElement("USERINFO'S JLIST");
        table.setValueAt("LS'S JTABLE", 0, 0);

        SwingUtilities.invokeLater(() -> 
        {
            this.setBounds(0, 0, 1200, 900);
            displayPrompt(textArea);
        });
        
        this.setVisible(true);
    }
    
    private void ignoreArrows(InputMap inputMap) 
    {
        String[] toIgnore = { "UP", "DOWN", "LEFT", "RIGHT", "HOME" };
        for (int i = 0; i < toIgnore.length; ++i)
            inputMap.put(KeyStroke.getKeyStroke(toIgnore[i]), "none");
    }

    private void displayPrompt(JTextArea textArea) 
    {
        textArea.setText(textArea.getText() + "> ");
    }

    private void showNewLine(JTextArea textArea) 
    {
        textArea.setText(textArea.getText() + lineSeparator);
    }

    private String executeCommand(String command) throws MyInvalidPathException 
    {
        return processor.processCommand(command);
    }

    private String extractCommand() 
    {
        removeLastLineSeparator();
        String newCommand = getCurrentCommand();
        return newCommand;
    }

    private void removeLastLineSeparator() 
    {
        String terminalText = textArea.getText();

        if (extractFail == false)
        {
            terminalText = terminalText.substring(0, terminalText.length());
            extractFail = true;
        }
        else
            terminalText = terminalText.substring(0, terminalText.length() - 1);

        textArea.setText(terminalText);
    }

    private String getCurrentCommand() 
    {
        String terminalText = textArea.getText();

        int lastPromptIndex = terminalText.lastIndexOf('>') + 2;
        if (lastPromptIndex < 0 || lastPromptIndex >= terminalText.length())
            return "";
        else
            return terminalText.substring(lastPromptIndex);
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) 
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private class KeyListener extends KeyAdapter 
    {
        private final int enter = KeyEvent.VK_ENTER;
        private final int backSpace = KeyEvent.VK_BACK_SPACE;
        private final int delete = KeyEvent.VK_DELETE;
        private final String backSpaceKeyBind = getKeyBinding(
                textArea.getInputMap(), "BACK_SPACE");

        private boolean isKeysDisabled;

        private String getKeyBinding(InputMap inputMap, String name) {
            return (String) inputMap.get(KeyStroke.getKeyStroke(name));
        }
        
        @Override
        public void keyPressed(KeyEvent event) 
        {
            super.keyPressed(event);
            
            if (firstRow)
                textArea.setCaretPosition(textArea.getText().length());
            else
                textArea.setCaretPosition(textArea.getDocument().getLength() - 1);

            
            int keyCode = event.getKeyCode();
            if (keyCode == backSpace) 
            {
                String terminalText = textArea.getText();
                int lastPromptIndex = terminalText.lastIndexOf('>') + 2;
                int cursorPosition = textArea.getCaretPosition();
                if (cursorPosition < lastPromptIndex && !isKeysDisabled) 
                {    
                    event.setKeyCode(backSpace);
                    disableBackspaceKey();
                }
                else if (cursorPosition > lastPromptIndex && isKeysDisabled) 
                    enableBackspaceKey();
                
            }
            else if (keyCode == enter) 
            {
                textArea.setEnabled(false);
                firstRow = false;
                textArea.grabFocus();
                
                String command = extractCommand();
                if (!command.matches("^ *"))
                {
                    StringTokenizer st = new StringTokenizer(command);
                    String first = st.nextToken();

                    if (command.contains("-POO"))
                    {
                        formatted = true;
                        command = command.substring(0, command.lastIndexOf(" "));
                    }
                else
                    formatted = false;
                    try {
                        if (!formatted)
                            textArea.append(executeCommand(command));
                        else
                        {
                            if (first.equals("userinfo"))
                                listModel.addElement(executeCommand(command));
                            if (first.equals("ls"))
                            {
                                toEnter = executeCommand(command);
                                String[] splitted = toEnter.split("\n");
                                for (String str : splitted)
                                    tableModel.addRow(new Object[]{str});
                            }
                            if (first.equals("echo"))
                            {
                                textAreaDialog.append(executeCommand(command));
                            }
                        }
                    } catch (MyInvalidPathException ex) {
                        Logger.getLogger(Terminal.class.getName())
                                .log(Level.SEVERE, null, ex);
                    }
                    
                    showNewLine(textArea);
                    displayPrompt(textArea);
                }                    
                textArea.setEnabled(true);
                table.setEnabled(false);
            }

        }
        
        public void close() 
        {
            frame.dispose();
        }

        public void clear() 
        {
            textArea.setText("");
            displayPrompt(textArea);
        }

        @Override
        public void keyReleased(KeyEvent event) 
        {
            super.keyReleased(event);
        }

        
        private void disableBackspaceKey() 
        {
            isKeysDisabled = true;
            textArea.getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"),
                    "none");
            textArea.getInputMap().put(KeyStroke.getKeyStroke("DELETE"),
                    "none");            
        }

        private void enableBackspaceKey() 
        {
            isKeysDisabled = false;
            textArea.getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"),
                    backSpaceKeyBind);
        }
    }
}