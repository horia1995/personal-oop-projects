/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temapoo;

import java.util.StringTokenizer;

/**
 *
 * @author HoriaAlexandru
 */
class CommandFactory 
{
    private CommandFactory() 
    {
    }

    public String processCommand(String command) throws MyInvalidPathException 
    {
        StringTokenizer st = new StringTokenizer(command);
        String name = st.nextToken();
        String params = "";
        if (st.hasMoreTokens())
            params = command.substring(name.length() + 1);
        
        params = params.replaceFirst("^ *", "");

        switch(name)
        {
            case "echo":
                Manager.echo(params);
                return Manager.toAdd;
            case "newuser":
                Manager.newuser(params);
                return Manager.toAdd;
            case "userinfo":
                return Manager.userinfo();
            case "login":
                Manager.login(params);
                return Manager.toAdd;
            case "logout":
                Manager.logout();
                return Manager.toAdd;
            case "cat":
                return new cat(params).toAdd;
            case "cd":
                return new cd(params).toAdd;
            case "ls":
                return new ls(params).toAdd;
            case "mkdir":
                return new mkdir(params).toAdd;
            case "pwd":
                return new pwd().toAdd;
            case "rm":
                return new rm(params).toAdd;
            case "touch":
                return new touch(params).toAdd;
            default:
                return "\n< Unknown command";
        }
    }

    public static CommandFactory getInstance() 
    {
        return INSTANCE;
    }

    private static final CommandFactory INSTANCE = new CommandFactory();
}